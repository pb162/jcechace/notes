# PB162 Notes for Seminar Group 14 and 15

This document contains notes for seminar groups 14 and 15. 

## Environment Setup

In this course you will need the following tools

- JDK 17
- Git VCS
- IntelliJ Idea IDE

Follow these steps in order to setup your computer

- [Faculty](https://pv168.pages.fi.muni.cz/guides/install-fi.html)
- [Unix/MacOS](https://pv168.pages.fi.muni.cz/guides/install-unix.html)
- [Windows](https://pv168.pages.fi.muni.cz/guides/install-windows)

### Git
You have likely encountered git already at the faculty. If not then the following resources might be helpful 

- [Nice and simple git tutorial](http://rogerdudler.github.io/git-guide/)
- [The official docs](https://www.git-scm.com/doc)

## Weekly Git Workflow
```bash
# Do these once
git config pull.rebase false                                                        # instructs git how to handle pulls
git config alias.next 'pull -X theirs --allow-unrelated-histories --no-edit'        # creates alias git next, giving you a simpler command for pulling next assignment
                                                                                    # pull from remote pb162 and branch iteration-03,  in case of conflict accepts their version and use default message for merge commit

# Every week (note: that the 'xy' in branch names should be replace with the name of current iteration)
git status                                                                          # should be clean
git switch main                                                                     # switch to main branch
git pull origin main                                                                # make sure local main is synced with origin main 
git next pb162 iteration-xy                                                         # get assignment for iterarion XY
git push origin main                                                                # push the assignment into  origin main assignment into  origin main 
git switch -c iteration-xy-impl                                                     # create new branch for implementation 

# After you are done with your work for the time
git status                                                                          # review which files are chnaged 
git add .                                                                           # prepare all changed and niew files in current directory for commit
git commit -m "Work on interation-xy"                                               # commit the changes, use meaningful message!
git push origin iteration-xy-impl                                                   # push the branch with solution to your remote repository (origin)


# If you want to resume your work on another comupter (first two steps are needed only once per machine)
git status                                                                          # check current branch, status shoudl be clean
git fetch origin                                                                    # fetch branch info from your remote repository (origin)
git switch iteration-xy-impl                                                        # switch to implementation branch

```

## Seminar Activities

1) Seminar project iterations
    - 10x 1 point 
2) Homework assignments
    - 3x pass. 
    - The goal is to reach acceptable implemnetation with good code structure and overfall code quality
    - The number of (re)submissions is not restricted, however I expect reasonable approach from your side  
3) Homework solution argumentation/ reasoning
    - 2x 3.5 points 
    - A short (1 - 2 standard pages) essay describing the line of line of thought behind your implementation and arguining it's strenghts and weaknesses. 
    - this mustn't be just a description of your implementation!
4) Remaining 3 points
    - Seminar activity 
    - Written feedback on exam assignment (1 standard page)
    - Homework or exam proposal 
